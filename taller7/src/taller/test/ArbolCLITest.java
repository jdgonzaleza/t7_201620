package taller.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import taller.interfaz.ArbolCLI;

public class ArbolCLITest {


	ArbolCLI<String> l = new ArbolCLI<String>();
	public void SetUp()
	{
		File archivo = new File("./data/caso1.properties");
		try {
			l.cargarArchivo(archivo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testCargarArchivo() {
		SetUp();
		assertEquals("A", l.darPre()[0]);
		int size = l.darIn().length;
		assertEquals("C".toString(), l.darIn()[size-2].toString());
		
	}

}
