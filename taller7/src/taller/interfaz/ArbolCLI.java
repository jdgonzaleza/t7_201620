package taller.interfaz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import org.json.JSONObject;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;


public class ArbolCLI<K> implements IReconstructorArbol
{
	private class Nodo<K>
	{
		K dato;
		Nodo<K> izq;
		Nodo<K> der;
		public Nodo(K valor)
		{
			dato = valor;
			izq = der = null;
		}
	}
	private Scanner in;
	private Properties datos;
	private String[] ino;
	private String[] pre;
	private Nodo<K> raiz;
	private int indice;


	public ArbolCLI()
	{
		in = new Scanner(System.in);
		indice = 0;
	}
	public Nodo<K> darRaiz()
	{
		return raiz;
	}
	public String[] darIn()
	{
		return ino;
	}
	public String[] darPre()
	{
		return pre;
	}

	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{	
			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();
			switch(opt1)
			{
			case 1:
				recibirArchivo();
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");

			String arch = in.next();
			File archivo = new File("./data/"+arch);

			// TODO Leer el archivo .properties 

			try {
				datos = cargarArchivo(archivo);
				pre = datos.getProperty("preorden").split(",");
				ino = datos.getProperty("inorden").split(",");
				reconstruir();
				crearArchivo("arbol.json");
				// TODO cargarArchivo
				// TODO Reconstruir árbol  

				System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");
				System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");
				System.out.println("Presione 1 para salir");
				in.next();
				finish=true;

			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}

	@Override
	public Properties cargarArchivo(File nombre) throws Exception {
		// TODO Auto-generated method stub
		datos = new Properties( );
		FileInputStream in = new FileInputStream( nombre );
		try
		{
			datos.load( in );
			in.close( );
		}
		catch(Exception e)
		{
			throw new Exception ("Formato invalido");
		}
		return datos;
	}
	@Override
	public void crearArchivo(String info) throws IOException {
		// TODO Auto-generated method stub
		
		File arch = new File(("./data/"+ info));
		if (!arch.exists())
		{
			try
			{
				arch.createNewFile();
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}
		}
		JSONObject json = new JSONObject();
		json = crearJSON(raiz);
		
		try (FileWriter archio = new FileWriter("./data/"+ info))
		{
			archio.write(json.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	@Override
	public void reconstruir() {
		// TODO Auto-generated method stub
		raiz = construirArbol(ino, pre, 0, pre.length-1);
		
	}
	public JSONObject crearJSON(Nodo<K> nodo) {
		JSONObject json = new JSONObject();
		List<JSONObject> hijos = new ArrayList<JSONObject>();
		if(nodo.der != null)
		{
			hijos.add(crearJSON(nodo.der));
		}
		if(nodo.izq != null)
		{
			hijos.add(crearJSON(nodo.izq));
		}
		json.put("Hijos", hijos);
		json.put("Padre", nodo.dato);
		return json;
	}
	public Nodo<K> construirArbol(String[] inorden, String[] preorden, int inicio, int fin)
	{
		
		if (indice> pre.length-1)
			return null;
		if(inicio>fin)
			return null;
		Nodo<K> nodo = new Nodo<K>((K) preorden[indice++]);
		if(inicio== fin)
			return nodo;
		int inorderInde = buscar(inorden, inicio, fin, nodo.dato);
		nodo.der = construirArbol(inorden, preorden, inorderInde+1, fin);
		nodo.izq = construirArbol(inorden, preorden, inicio, inorderInde-1);
		
		return nodo;
	}
	public boolean subArbol(Nodo<K> p, Nodo<K> h)
	{
		if (p == null)
			return false;
		if(h == null)
			return true;
		if(iguales(p, h))
			return true;
		return subArbol(p.der,  h)||subArbol(p.izq, h); 
	}
	
	public boolean esHijo(Nodo<K> hijo, Nodo<K> nodo)
	{
		return nodo.izq == hijo|| nodo.der == hijo;
	}
	private int buscar(String[] arr, int inicio, int fin, K dato)
	{
		int indice = -1;
		for (int i = inicio; i<= fin; i++)
		{
			if(arr[i].equals(dato))
				indice = i;
		}
		return indice;
	}
	private boolean iguales(Nodo<K> n1, Nodo<K> n2)
	{
		if(n1 == null || n2 == null)
			return false;
		if(n1 == null && n2 == null)
			return true;
		else
		return (n1.dato == n2.dato && iguales(n1.der, n2.der)&& iguales(n1.izq, n2.izq));
	}
	



}