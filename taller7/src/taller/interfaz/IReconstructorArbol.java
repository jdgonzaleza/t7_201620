package taller.interfaz;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public interface IReconstructorArbol {

	/**
	 * Este es el método que carga el archivo
	 * @param nombre es el nombre del archivo a cargar (con .properties incluido)
	 * @throws IOException si no se puede cargar el archivo
	 * @throws Exception 
	 */
	public  Properties cargarArchivo(File nombre) throws Exception, Exception;


	/**
	 * Este método crea el archivo con el árbol en formato JSON
	 * @param info el método crea el archivo con esta información dentro
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws IOException 
	 */
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException, IOException;
	
	/**
	 * Este es el método que reconstruye el árbol.El inorden y postorden son
	 * atributos del mundo.
	 */
	public void reconstruir();


}
